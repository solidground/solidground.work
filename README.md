# Solidground • Floorplanner • Groundwork

Solidground Social Experience Design for the Fediverse. Main project website.

## Code of Conduct

As [Social Coding](https://coding.social) practitioners we adhere to [Social Coding Principles](https://coding.social/principles) and follow the [Social Coding Community Participation Guidelines](CODE_OF_CONDUCT.md).

## Credits

Based on the [Jekynewage](https://github.com/jekynewage/jekynewage.github.io) theme by [Antonio Trento](https://github.com/antoniotrento). Artwork credits to [Manypixels](https://www.manypixels.co/gallery/). Website created by [Arnold Schrijver](https://mastodon.social/@humanetech).

## License

[AGPL-3.0](LICENSE) for project code, and [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) for documentation.